# Web Filter

Do you find yourself wasting hours not being productive? Checking social media often? Watching too many videos? Install my Website Filter, and that wont happen. You customize the sites to block and of course the hours of operation, set up with cron or execute in shell.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Admin privileges
Knowledge of cron and setting crontab job to run as root

### Installing

Configure web_filter.py for the hours of operation (8am - 4pm)

Launch nano opening crontab as root, type below:
```
sudo env EDITOR=nano crontab -e
```
Add line to crontab and save:
Change the paths to point to python and the location of the web_filter.py
```
@reboot  /path/to/python  /path/to/web_filter.py
```
Real World Example:
```
@reboot  /Library/Frameworks/Python.framework/Versions/3.6/bin/python3  /Users/kerberos/webBlocker/web_filter.py
```
Reboot or wait for the next system reboot.

## Deployment

Web Filter works for OS X, Linux and Windows

## Built With

Python 3

## Versioning

1.0.0

## Authors

* **k3rber0s**
