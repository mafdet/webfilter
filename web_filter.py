import time
from datetime import datetime as dt
'''
Name: webFilter.py
Author: k3rber0s
date: 09/04/2017
email: k3rber0s.c3rber0s@gmail.com
'''
class Blocker():

    def __init__(self):
        ''' Duration: '''
        # Begin & End: Example given 8am and 4pm based on 24hour cycle
        self.begin = 9
        self.end = 17
        ''' Hosts for different operating systems:
        Comment out (#) the OS not in use, default is OSX/Linux
            Testing: '''
        #self.hosts_temp = "hosts"
        ''' Windows: '''
        #self.windows_host = r"C:\Windows\System32\drivers\etc\hosts"
        #self.host_path = self.windows_host
        ''' macOS or Linux: '''
        self.mac_linux_host = "/etc/hosts"
        self.hosts_path = self.mac_linux_host

        ''' IP address & site list: '''
        # Redirects the websites in list to localhost
        # IP Address - default is localhost:
        self.ip_address = "127.0.0.1"
        # Websites to block:
        # ToDo: accept file or db hook in
        self.block_list = ["www.facebook.com", "facebook.com", "www.twitter.com", "twitter.com", "www.espn.com", "espn.com", "www.yahoo.com", "yahoo.com", "www.kabam.com", "kabam.com"]

    # Block website: writes into hosts file each website in website_list with redirect before it.
    def block_website(self):
        with open(self.hosts_path, 'r+') as file:
            content = file.read()
            for site in self.block_list:
                if site in content:
                    pass
                else:
                    file.write(self.ip_address + " " + site + "\n")

    # Unblock website: removes entries from host file.
    def unblock_website(self):
        with open(self.hosts_path, 'r+') as file:
            content = file.readlines()
            file.seek(0)
            for line in content:
                if not any(site in line for site in self.block_list):
                    file.write(line)
            file.truncate()

blocker = Blocker()
while True:
    # Basically stating if time falls between 8am and 4pm (default), based on begin & end
    if dt(dt.now().year, dt.now().month, dt.now().day, blocker.begin) < dt.now() < dt(dt.now().year, dt.now().month, dt.now().day, blocker.end):
        blocker.block_website()
    else:
        blocker.unblock_website()

    time.sleep(60)
